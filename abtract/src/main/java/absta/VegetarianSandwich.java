package absta;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
public abstract class VegetarianSandwich implements ISandwich{

    private String filling;

    public  VegetarianSandwich(){
       this.filling="";
    }
    abstract String getProtein();

    @Override
    public void addFilling(String topping) {
        // TODO Auto-generated method stub
        String[] arrayOfUnwanted = new String[]{"chicken", "beef" ,"fish", "meat", "pork" };
        for(int i=0; i <arrayOfUnwanted.length;i++){
            if(topping.equals(arrayOfUnwanted[i])){
                throw new UnsupportedOperationException("Unimplemented method 'addFilling'");
            } 
        }
        this.filling+=topping;
    }

    @Override
    public boolean isVegetarian() {
       return true;
    }
    
    public boolean isVegan(){
        
        String word1 = "cheese";
        String word2 = "egg";

        Pattern pattern = Pattern.compile("\\b" + Pattern.quote(word1) + "\\b|\\b" + Pattern.quote(word2) + "\\b");

        Matcher matcher = pattern.matcher(this.filling);
        if (matcher.find()) {
            System.out.println("The sentence contains either \"" + word1 + "\" or \"" + word2 + "\".");
             return false;
        } else {
            System.out.println("The sentence does not contain either \"" + word1 + "\" or \"" + word2 + "\".");
             return true;
        }
        
       
    }
}
