package absta;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        // VegetarianSandwich sandwich = new VegetarianSandwich();
            // This will cause a compile-time error
        ISandwich sandwich = new TofuSandwich();
            // This works
        TofuSandwich tofuSandwich = new TofuSandwich();
        if (tofuSandwich instanceof ISandwich) {
            System.out.println("TofuSandwich is an instance of ISandwich");
        } else {
            System.out.println("TofuSandwich is not an instance of ISandwich");
        }
            // Its true

        if (tofuSandwich instanceof VegetarianSandwich) {
            System.out.println("TofuSandwich is an instance of Vegetarian");
        } else {
            System.out.println("TofuSandwich is not an instance of Vegetarian");
        }
            // Also true
        tofuSandwich.addFilling("chicken");
            // yep throws exception
        tofuSandwich.isVegan();
            // yep compiles
        
    }
}
