package absta;

import static org.junit.Assert.*;

import org.junit.Test;


public class VegetarianSandwichTest {
    @Test
    public void testGetFilling(){
        VegetarianSandwich vs = new VegetarianSandwich();
        assertEquals("Empty ","",vs.getFilling());
    }
    @Test
    public void testFaillingAddFilling(){
        VegetarianSandwich vs = new VegetarianSandwich();
        try{
            vs.addFilling("pork");
            fail();
        }catch(UnsupportedOperationException e){

        }
    }
    @Test
    public void testWorkingAddFilling(){
            VegetarianSandwich vs = new VegetarianSandwich();
            vs.addFilling("lettuce");
            assertEquals("Same topping ","lettuce",vs.getFilling());
    }
    @Test
    public void testIsVegetarian(){
        VegetarianSandwich vs = new VegetarianSandwich();
        assertEquals("true stuff ",true,vs.isVegetarian());
    }
}
    